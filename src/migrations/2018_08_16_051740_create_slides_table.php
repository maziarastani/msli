<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slides', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title');
            $table->text('body')->nullable();
            $table->json('data')->nullable();
            $table->unsignedInteger('topic_id')->nullable();
            $table->enum('type', ['html', 'quiz' , 'video']);
            $table->integer('duration')->nullable();
            $table->boolean('can_skip')->default(false);
            $table->index('topic_id');
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slides') ;
    }
}

<?php

namespace Jahan\Slide;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = ['title','data'];
    protected $table = 'courses';
    public $timestamps = true;

    public function chapter()
    {
        return $this->belongsToMany('Jahan\Slide\Chapter');
    }
}

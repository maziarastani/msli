<?php

namespace Jahan\Slide;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $fillable = ['title'];
    protected $table = 'topics';
    public $timestamps = true;

    public function slide()
    {
        return $this->hasMany('Jahan\Slide\Slide','topic_id');
    }

    public function chapter()
    {
        return $this->belongsToMany('Jahan\Slide\Chapter');
    }


}

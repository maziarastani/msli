<?php

namespace Jahan\Slide;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{

    protected $fillable = ['title'];
    protected $table = 'chapters';
    public $timestamps = true;

    public function topic()
    {
        return $this->belongsToMany('Jahan\Slide\Topic');
    }

    public function course()
    {
        return $this->belongsToMany('Jahan\Slide\Course');
    }

}

<?php

namespace Jahan\Slide;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $fillable = [ 'title', 'body', 'data','type', 'duration','can_skip' , 'topic_id'];
    protected $table = 'slides';
    public $timestamps = true;
    protected $primaryKey = "id";

    public function topic(){
        return $this->belongsTo('Jahan\Slide\Topic');
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: maziarastani
 * Date: 8/16/2018 AD
 * Time: 09:24
 */
Route::resource('slide', 'Jahan\Slide\SlideController');
Route::resource('topic', 'Jahan\Slide\TopicController');
Route::resource('chapter', 'Jahan\Slide\ChapterController');
Route::resource('course', 'Jahan\Slide\CourseController');

Route::get('slide/create/{topic_id}', 'Jahan\Slide\SlideController@create');

<?php

namespace Jahan\Slide;

use Collective\Html\HtmlFacade as HTML;
use Collective\Html\FormFacade as Form;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Jahan\Slide\Course;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::all();
        return view('slide::course.index', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $chapters = Chapter::all();
        $compactData = array('chapters');
        return view('slide::course.course', compact($compactData));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $course = new Course();
        $course->title = $request->title;
        $course->save();
        foreach ($request->chapter as $key => $si) {
            $chapter = Chapter::find($key);
            $course->chapter()->attach($chapter);
        }

        return Redirect::to('course');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\course $course
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $course = Course::findOrFail($id);
        $chapters = $course->chapter;
        $compactData = array('course', 'chapters');
        return view('slide::course.show',compact($compactData));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\course $course
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::findOrFail($id);
        $chapters = Chapter::all();
        $compactData = array('course', 'chapters');
        return view('slide::course.course', compact($compactData));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\course $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $course = Course::findOrFail($id);
        $course->title = $request->title;
        $course->chapter()->detach();

        foreach ($request->chapter as $key => $si) {
            $chapter = Chapter::find($key);
            $course->chapter()->attach($chapter);
        }

        return Redirect::to('course');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\course $course
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = Course::findOrFail($id);
        $course->delete();
        return Redirect::to('course');
    }
}

<?php

namespace Jahan\Slide;

use Collective\Html\HtmlFacade as HTML;
use Collective\Html\FormFacade as Form;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use getID3;
use Jahan\Slide\Topic;
class SlideController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($topic_id)
    {
        $topic = Topic::findOrFail($topic_id);
        return view('slide::slide.slide',compact('topic'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function slideBuild(Request $request, Slide $slide)
    {


//        print_r($request->htmlbody);

        $slide->title = $request->title;
        $slide->duration = $request->duration;

        switch ($request->type) {
            case "html":
//                if($slide->type == "video" ){
//                    $slide->video()->delete();
//                }
//
                $slide->type = "html";
                $detail = $request->slideBody;
                if (!empty($detail)) {

                    $dom = new \domdocument();
                    $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

                    $images = $dom->getelementsbytagname('img');

                    foreach ($images as $k => $img) {
                        $data = $img->getattribute('src');

                        list($type, $data) = explode(';', $data);
                        list(, $data) = explode(',', $data);
//
                        $data = base64_decode($data);
                        $image_name = time() . $k . '.png';
                        $path = public_path() . '/img/' . $image_name;

                        file_put_contents($path, $data);

                        $img->removeattribute('src');
                        $img->setattribute('src', '/img/' . $image_name);
                    }

                    $detail = $dom->savehtml();
                } else {
                    $detail = "";
                }

                $slide->body = $detail;

                break;
            case "video":


                $slide->type = "video";
                $video = new \stdClass();
                $video->link = $request->link;
                $video->quality = $request->quality;
                $video->width = $request->width;
                $video->height = $request->height;
                $video->duration = $request->videoDuration;

                $data = json_encode(['video' => $video]);


                $slide->data = $data;


                $body = '
                 <iframe src="' . $video->link . '" 
                 width="' . $video->width . '" height="' . $video->height . '" frameborder="0"  autostart="false" webkitallowfullscreen mozallowfullscreen allowfullscreen>
                 
</iframe>
                 ';

                $slide->body = $body;
//                $video = new Video();
//                $video->link = $request->link;
//                $video->quality = $request->quality;
//                $video->width = $request->width;
//                $video->height = $request->height;
//                $video->duration = $request->videoDuration;
//                $video->slide()->associate($slide);
//                $video->save();

                break;
            case "quiz":
                $slide->type = "quiz";


                $quiz = new \stdClass();
                $quiz->question = $request->question;
                $items = [];

                foreach ($request->answer['text'] as $key => $answer) {
                    if (!$answer == "") {
                        $quizItem = new \stdClass();
                        $quizItem->answer_text = $answer;
                        if (isset($request->answer['is'][$key])) {

                            if ($request->answer['is'][$key] == "on") {
                                $quizItem->is_correct = true;
                            } else {
                                $quizItem->is_correct = false;
                            }
                        } else {

                            $quizItem->is_correct = false;

                        }
                        array_push($items, $quizItem);

                    }

                }

                $quiz->items = $items;

                $slide->data = json_encode(['quiz' => $quiz]);
                $option = "";
                $i = 0;
                foreach ($items as $item) {
                    $option = $option . '<p> <input id = "a' . $i . '" type = "checkbox" > - <span >
                    ' . $item->answer_text . '
                    </span > </p >';
                    $i = $i + 1;

                }
                $body = '
                
              <div id="quiz-show">
              ' . $option . '
                    
                    <p class="btn btn-secondary" onclick="quizCh(this)">submit</p><br>
                </div>

                <script>
                    var items = [];               
                       items= ' . json_encode($items) . ';

                    function quizCh(btn) {
                        var res = [];
                        var point = 0;
                        var cor = [];
                        var inc = [];
                        for (i = 0; i < items.length; i++) {
                            var ch = document.getElementById(\'a\' + i);
                            if(items[i][\'is_correct\']){
                                ch.parentElement.style.color = \'#20c997\';
                                cor.push(1);
                            }
                            if (ch.checked && items[i][\'is_correct\']) {
                                ch.parentElement.style.color = \'#007bff\';
                                res.push(1);
                            }else if(ch.checked){
                                ch.parentElement.style.color = \'#dc3545\';
                                inc.push(1);
                            }else if(!ch.checked && ! items[i][\'is_correct\']){

                            }
                        }
                        btn.setAttribute(\'onclick\',"");
                        var los = (100 / items.length) * inc.length;
                        var add = (100 / cor.length) * res.length;
                        point = point + add - los ;
                        btn.parentElement.appendChild(document.createTextNode("your point is: "+ point))
                    }
                </script>
                
                
                ';

                $slide->body = $body;

//                $quiz = new Quiz();
//                $quiz->question = $request->question;
//                $quiz->slide()->associate($slide);
//                $quiz->save();
//                foreach ($request->answer['text'] as $key => $answer){
//                    if(!$answer == "") {
//                        $quizItem = new QuizItem();
//                        $quizItem->answer_text = $answer;
//                        if (isset($request->answer['is'][$key])) {
//
//                            if ($request->answer['is'][$key] == "on") {
//                                $quizItem->is_correct = true;
//                            } else {
//                                $quizItem->is_correct = false;
//                            }
//                        }else {
//
//                            $quizItem->is_correct = false;
//
//                        }
//                        $quizItem->quiz()->associate($quiz);
//                        $quizItem->save();
//
//                    }
//
//                }

                break;
        }
        switch ($request->can_skip) {
            case "true":
                $slide->can_skip = true;
                break;
            default:
                $slide->can_skip = false;
        }
        return $slide;
    }

    public function store(Request $request)
    {
//        $validatedData = $request->validate([
//            'title' => 'required',
//            'duration' => 'numeric',
//            'video_id' => 'numeric',
//
//        ]);

        $slide = new Slide();
        $topic = Topic::findOrFail($request->topic_id);
        $slide->topic()->associate($topic);
        $slide = $this->slideBuild($request, $slide);
        $slide->save();
        return Redirect::to('slide');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $slides = Slide::all();
        return view('slide::slide.index', compact('slides'));


    }


    /**
     * Display the specified resource.
     *
     * @param  \Jahan\Slide $slide
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $slide = Slide::findOrFail($id);
        return view('slide::slide.show')->with('slide', $slide);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Jahan\Slide $slide
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slide = Slide::findOrFail($id);
        $topic = $slide->topic;
        $compactData = array('slide', 'topic');

        return view('slide::slide.slide', compact($compactData));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Jahan\Slide $slide
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
//        $validatedData = $request->validate([
//            'title' => 'required',
//            'duration' => 'numeric',
//            'video_id' => 'numeric',
//
//        ]);

        $slide = Slide::findOrFail($id);
        $slide = $this->slideBuild($request, $slide);
        $slide->save();
        return Redirect::back();
//        return Redirect::to('slide');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Jahan\Slide $slide
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slide = Slide::findOrFail($id);
        $slide->delete();
        return Redirect::to('slide');
    }

    public function getVideoInfo()
    {
        $remotefilename = $_POST['link'];
        try {

            if ($fp_remote = fopen($remotefilename, 'rb')) {
                $localtempfilename = tempnam('/tmp', 'getID3');
                if ($fp_local = fopen($localtempfilename, 'wb')) {
                    while ($buffer = fread($fp_remote, 8192)) {
                        fwrite($fp_local, $buffer);
                    }
                    fclose($fp_local);
                    // Initialize getID3 engine
                    $getID3 = new getID3;
                    $ThisFileInfo = $getID3->analyze($localtempfilename);
                    // Delete temporary file
                    unlink($localtempfilename);
                }
                fclose($fp_remote);
            }
        } catch (Exception $e) {

            return json_encode(['error', $e]);


        }

        if (!empty($ThisFileInfo)) {
            return json_encode($ThisFileInfo);
        } else {
            return json_encode(['error', 'problem with geting video info']);
        }

    }


}

<?php

namespace Jahan\Slide;

use Collective\Html\HtmlFacade as HTML;
use Collective\Html\FormFacade as Form;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Jahan\Slide\Chapter;

class ChapterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $chapters = Chapter::all();
        return view('slide::chapter.index', compact('chapters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $topics = Topic::all();
        $compactData = array('topics');
        return view('slide::chapter.chapter', compact($compactData));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $chapter = new Chapter();
        $chapter->title = $request->title;
        $chapter->save();
        foreach ($request->topic as $key => $si) {
            $topic = Topic::find($key);
            $chapter->topic()->attach($topic);
        }

        return Redirect::to('chapter');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\chapter $chapter
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $chapter = Chapter::findOrFail($id);
        $topics = $chapter->topic;
        $compactData = array('chapter', 'topics');
        return view('slide::chapter.show',compact($compactData));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\chapter $chapter
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $chapter = Chapter::findOrFail($id);
        $topics = Topic::all();
        $compactData = array('chapter', 'topics');
        return view('slide::chapter.chapter', compact($compactData));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\chapter $chapter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $chapter = Chapter::findOrFail($id);
        $chapter->title = $request->title;
        $chapter->topic()->detach();

        foreach ($request->topic as $key => $si) {
            $topic = Topic::find($key);
            $chapter->topic()->attach($topic);
        }

        return Redirect::to('chapter');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\chapter $chapter
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $chapter = Chapter::findOrFail($id);
        $chapter->delete();
        return Redirect::to('chapter');
    }
}

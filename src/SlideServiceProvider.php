<?php

namespace Jahan\Slide;

use Illuminate\Support\ServiceProvider;



class SlideServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__ . '/routes.php';
        $this->loadMigrationsFrom(__DIR__ . '/migrations');
        $this->mergeConfigFrom(
            __DIR__.'/config/app.php', 'app'
        );

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

//        $this->app->bind('slide', function ($app) {
//            return new Slide;
//        });
        $this->app->make('Jahan\Slide\SlideController');
        $this->app->make('Jahan\Slide\ChapterController');
        $this->app->make('Jahan\Slide\TopicController');
        $this->app->make('Jahan\Slide\CourseController');
        $this->loadViewsFrom(__DIR__ . '/views', 'slide');
        $this->publishes([
//            __DIR__ . '/views' => resource_path('views/slide'),
//            __DIR__ . '/migrations' => base_path("database/migrations"),
            __DIR__ . '/resources/assets' => base_path('public'),
        ]);

    }
}

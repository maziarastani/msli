<?php
/**
 * Created by PhpStorm.
 * User: maziarastani
 * Date: 9/4/2018 AD
 * Time: 02:28
 */

return [
    'providers' => [
        Jahan\Slide\SlideServiceProvider::class,
        Collective\Html\HtmlServiceProvider::class,
    ]
    ,
    'aliases' => [
        'Form' => Collective\Html\FormFacade::class,
        'Html' => Collective\Html\HtmlFacade::class,
        'Input' => \Illuminate\Support\Facades\Input::class,

        ]
];
@extends('slide::slide-master')
@section('title', 'Courses')


@section('content')

    @foreach ($courses as $course)
        <div class="card">
            <div class="card-header">
                course {{$course->id}}
            </div>
            <div class="card-body">
                <h5 class="card-title">{{$course->title}}</h5>
                <a href="/course/{{$course->id}}" class="btn btn-primary">Show</a>
                <a href="/course/{{$course->id}}/edit" class="btn btn-outline-primary">Edit</a>
            </div>
        </div>

    @endforeach

@endsection

@extends('slide::slide-master')
@if(!empty($course))
    @section('title', 'course - Edit course')

@else
    @section('title', 'course - Create course')

@endif

@section('content')

    @if(!empty($course))
        {{ Form::model($course, array('route' => array('course.update', $course->id), 'method' => 'PUT')) }}


    @else
        {{ Form::open(array('url' => 'course')) }}

    @endif

    @php

        if(!isset($course) || empty($course->data)){
                $data = new stdClass();
        }else{
                $data = json_decode($course->data);
        }



    @endphp


    <div class="form-group">
        {{ Form::label('title', 'Title') }}
        {{ Form::text('title', Input::old('title') , array('class' => 'form-control','placeholder' => 'Title','required'=>true)) }}
    </div>
    {{ Form::label('chapters', 'chapters') }}
    <div class="form-group" id="chapters">
        {{--@php--}}
            {{--$to = $course->chapter;--}}

        {{--var_dump($to);--}}

        {{--@endphp--}}
        @foreach($chapters as $item)

            @php

                $checked = "";
                if(!empty($course)){
            if($item->course->contains($course->id)){
            $checked = "checked";
            }
            }

            @endphp
            <p><input name="chapter[{{$item->id}}]" type="checkbox" {{$checked}}> -
                <span>{{$item->title}}</span></p>

        @endforeach


    </div>


    @if(!empty($course))
        <div class="row">
            <div class="col">
                {{ Form::submit('Edit course', array('class' => 'btn btn-primary')) }}
                {{ Form::close() }}
            </div>
            <div class="col">
                {{ Form::open(array('url' => 'course/' . $course->id, 'class' => 'pull-right')) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit('Delete this course', array('class' => 'btn btn-warning')) }}
                {{ Form::close() }}
            </div>
        </div>

    @else
        {{ Form::submit('Create course', array('class' => 'btn btn-primary')) }}
        {{ Form::close() }}

    @endif




@endsection
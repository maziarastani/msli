@extends('slide::slide-master')
@section('title', 'Course')


@section('content')
<h1>{{$course->title}}</h1>
<div class="list-group">
@foreach($chapters as $chapter)

        <h2>{{$chapter->title}}</h2>
        <div class="list-group">
            @php
            $topics = $chapter->topic;
            @endphp
            @foreach($topics as $topic)

                <a href="/topic/{{$topic->id}}" class="list-group-item list-group-item-action">{{$loop->index +1}}. {{$topic->title}}</a>

            @endforeach
        </div>
@endforeach
</div>

@endsection
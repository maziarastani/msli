@extends('slide::slide-master')
@section('title', 'Slides')

@section('top-script')
    <script src='/js/html2canvas.js'></script>
@endsection

@section('content')

    @foreach ($slides as $slide)
        @if($loop->index % 2 == 0)
            <div class="row" style="margin: auto">
                @endif
                <div class="col-lg-5 col-md-5 col-sm-12 col-xl-5 slide btn btn-outline-secondary">
                    @if($slide->type != "video")
                    <div id="{{$slide->id}}" class="thumb-c">

                        {!!$slide->body!!}
                    </div>
                    @else

                        <div id="{{$slide->id}}" class="thumb-c">
<br>
<br>
<br>
                            <h1>Video</h1>
                        </div>
                    @endif
                    <div id="s{{$slide->id}}" class="thumb btn">

                    </div>
                    <div class="buttons">
                        <span>For Topic : {{$slide->topic->title}} | {{$slide->title}} </span>
                        <a href="/slide/{{ $slide->id }}/edit" class="btn btn-outline-dark">Edit</a>
                        <a href="/slide/{{$slide->id}}" target="_blank" class="btn btn-primary">Show</a>

                    </div>
                    <script>
                        html2canvas(document.getElementById("{{$slide->id}}")).then(canvas => {
                            document.getElementById("{{$slide->id}}").style.display = "none";
                            document.getElementById("s{{$slide->id}}").appendChild(canvas);
                        });
                    </script>

                </div>

                @if($loop->index % 2 != 0 || $loop->remaining == 0)
            </div>
        @endif

    @endforeach

@endsection

<html>
<head>
    <title>Slide - {{$slide->title}} </title>
    <script src='/js/html2canvas.js'></script>

    <link rel="stylesheet" href="/css/reveal.css">
    <link rel="stylesheet" href="/css/theme/white.css">
    <link rel="stylesheet" href="/css/bootstrap.css">

</head>
<body>

<div class="reveal">
    <div class="slides">
        <section>
            <h1>{!! $slide->title !!}</h1>
            {!! $slide->body !!}
        </section>


    </div>
</div>
<script src="/js/reveal.js"></script>
<script>
    Reveal.initialize({
        autoPlayMedia: false,
        defaultTiming: 120

    });
</script>


</body>
</html>
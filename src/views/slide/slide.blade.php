@extends('slide::slide-master')

@section('top-script')


    <link href="/css/quill.css" rel="stylesheet">
    <script src="/js/quill.js"></script>

@endsection

@section('content')


    @if(!empty($slide))
@section('title', 'Slide - Edit Slide')

{{ Form::model($slide, array('route' => array('slide.update', $slide->id), 'method' => 'PUT' , 'onsubmit' =>'return divToForm()') ) }}

@php
    $body = $slide->body;
    $data = json_decode($slide->data);

    if(empty($data)){
            $data = new stdClass();
    }



@endphp
@else
    @section('title', 'Slide - Create Slide')

{{ Form::open(array('url' => '/slide/','onsubmit' =>'return divToForm()')) }}
@php

    $body = "";

$data = new stdClass();

@endphp
@endif

<h3 class="">For Topic : {{$topic->title}} </h3>


<div class="form-group">
    {{ Form::label('title', 'Title') }}
    {{ Form::text('title', Input::old('title') , array('class' => 'form-control','placeholder' => 'Title','required'=>true)) }}
</div>

<div class="row">

    <div class="form-group col">
        {{ Form::label('type', 'Type') }}
        {{ Form::select('type', array('html' => 'html', 'quiz' => 'quiz', 'video' => 'video'), Input::old('type'), array('class' => 'form-control','onchange'=>'typeSel(this)','id'=>'sel','required'=>true)) }}

    </div>

    <div class="form-group col">
        {{ Form::label('duration', 'Duration-sec') }}
        {{Form::number('duration', Input::old('duration') , ['placeholder' => 'Duration-s','class' => 'form-control'])}}
    </div>

</div>

<div class="form-group" id="html">
    {{ Form::label('slideBody', 'HTML') }}


    <div id="toolbar-container">
        <span class="ql-formats">
<select class="ql-font"></select>
<select class="ql-size"></select>
</span>
        <span class="ql-formats">
<button class="ql-bold"></button>
<button class="ql-italic"></button>
<button class="ql-underline"></button>
<button class="ql-strike"></button>
</span>
        <span class="ql-formats">
<select class="ql-color"></select>
<select class="ql-background"></select>
</span>
        <span class="ql-formats">
<button class="ql-script" value="sub"></button>
<button class="ql-script" value="super"></button>
</span>
        <span class="ql-formats">
<button class="ql-header" value="1"></button>
<button class="ql-header" value="2"></button>
<button class="ql-blockquote"></button>
<button class="ql-code-block"></button>
</span>
        <span class="ql-formats">
<button class="ql-list" value="ordered"></button>
<button class="ql-list" value="bullet"></button>
<button class="ql-indent" value="-1"></button>
<button class="ql-indent" value="+1"></button>
</span>
        <span class="ql-formats">
<button class="ql-direction" value="rtl"></button>
<select class="ql-align"></select>
</span>
        <span class="ql-formats">
<button class="ql-link"></button>
<button class="ql-image"></button>
<button class="ql-video"></button>
<button class="ql-formula"></button>
</span>
        <span class="ql-formats">
<button class="ql-clean"></button>
</span>
    </div>
    <div id="editor-container" height="300">

    </div>


    {{ Form::text('slideBody', null, ['id' => 'slideBody','style' => 'display:none !important']) }}
</div>
@if(!isset($data->video))
    @php
        $video = new stdClass();
$video->link = "";
$video->quality = "";
$video->width = "";
$video->height = "";
$video->duration = "";
            $data->video = $video

    @endphp
@endif
<div class="form-group" id="video" style="display: none">
    {{ Form::label('slideBody', 'video') }}

    <div class="form-row">


        <div class="form-group col-md-8">
            {{ Form::label('link', 'Link') }}
            {{Form::text('link', $data->video->link , ['placeholder' => 'Link','class' => 'form-control'])}}
        </div>

        <div class="form-group col-md-4">
            {{ Form::label('quality', 'Quality') }}
            {{Form::text('quality', $data->video->quality , ['placeholder' => 'Quality','class' => 'form-control'])}}
        </div>


    </div>

    <div class="row">

        <div class="form-group col">
            {{ Form::label('width', 'Width') }}
            {{Form::number('width', $data->video->width , ['placeholder' => 'Width','class' => 'form-control'])}}
        </div>
        <div class="form-group col">
            {{ Form::label('height', 'Height') }}
            {{Form::number('height', $data->video->height , ['placeholder' => 'Height','class' => 'form-control'])}}
        </div>
        <div class="form-group col">
            {{ Form::label('videoDuration', 'Video Duration-sec') }}
            {{Form::number('videoDuration', $data->video->duration , ['placeholder' => 'Video Duration-sec','class' => 'form-control'])}}
        </div>


    </div>

</div>


<div class="form-group" id="quiz" style="display: none">
    <div class="input-group">
        <div class="input-group-prepend">
            <div class="input-group-text">?</div>
        </div>
        {{--{{ Form::label('question', 'Question') }}--}}

        @if(!isset($data->quiz))
            @php
                $quiz = new stdClass();
        $quiz->question = "";
        $items = [];
        $quiz->items = $items;
        $data->quiz = $quiz;

            @endphp
        @endif

        {{Form::text('question',$data->quiz->question,['placeholder'=> 'Question','id' => 'question','class'=>'form-control'])}}
    </div>
    <br>
    <p onclick="row()" class="btn btn-outline-info">Add Answer</p>

    <table id="tb">
        <thead>
        <tr>
            <th></th>
            <th>Correct?</th>
            <th>Answers</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @if(isset($slide))
            @if($slide->type == "quiz")





                @foreach($data->quiz->items as $item)
                    @php
                        $checked = "";
                        if($item->is_correct){
                        $checked = "checked";
                        }
                    @endphp
                    <tr>
                        <td>{{$loop->index + 1}}</td>
                        <td><input name="answer[is][{{$loop->index + 1}}]" type="checkbox" {{$checked}}></td>
                        <td><input type="text" value="{{$item->answer_text}}"
                                   name="answer[text][{{$loop->index + 1}}]"
                                   placeholder="Answer" class="form-control">
                        </td>
                        <td>

                            @if($loop->index >1)
                                <input type="button" class="btn btn-warning" value="X"
                                       onClick="delet({{$loop->index + 1}})"/>
                            @endif

                        </td>
                    </tr>

                @endforeach




            @else

                <tr>
                    <td>1</td>
                    <td><input name="answer[is][1]" type="checkbox"></td>
                    <td><input type="text" name="answer[text][1]" placeholder="Answer" class="form-control">
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td><input name="answer[is][2]" type="checkbox"></td>
                    <td><input type="text" name="answer[text][2]" placeholder="Answer" class="form-control">
                    </td>
                    <td></td>
                </tr>

        </tbody>

        @endif

        @else

            <tr>
                <td>1</td>
                <td><input name="answer[is][1]" type="checkbox"></td>
                <td><input type="text" name="answer[text][1]" placeholder="Answer" class="form-control">
                </td>
                <td></td>
            </tr>
            <tr>
                <td>2</td>
                <td><input name="answer[is][2]" type="checkbox"></td>
                <td><input type="text" name="answer[text][2]" placeholder="Answer" class="form-control">
                </td>
                <td></td>
            </tr>

            </tbody>

        @endif

    </table>
    {{--@if(isset($slide))--}}
    {{--@if($slide->type == "quiz")--}}

    {{--<div id="quiz-show">--}}
    {{--@foreach($data->quiz->items as $key => $item)--}}
    {{--<p> <input id="a{{$key}}" type="checkbox"> - <span>{{$item->answer_text}}</span> </p>--}}
    {{--@endforeach--}}
    {{--<p class="btn btn-secondary" onclick="quizCh(this)">submit</p>--}}
    {{--</div>--}}

    {{--<script>--}}
    {{--var items = [];--}}

    {{--@php--}}
    {{--echo "items = ". json_encode($data->quiz->items) .";";--}}

    {{--@endphp--}}


    {{--function quizCh(btn) {--}}
    {{--var res = [];--}}
    {{--var point = 0;--}}
    {{--var cor = [];--}}
    {{--var inc = [];--}}
    {{--for (i = 0; i < items.length; i++) {--}}
    {{--var ch = document.getElementById('a' + i);--}}
    {{--if(items[i]['is_correct']){--}}
    {{--ch.parentElement.style.color = '#20c997';--}}
    {{--cor.push(1);--}}
    {{--}--}}
    {{--if (ch.checked && items[i]['is_correct']) {--}}
    {{--ch.parentElement.style.color = '#007bff';--}}
    {{--res.push(1);--}}
    {{--}else if(ch.checked){--}}
    {{--ch.parentElement.style.color = '#dc3545';--}}
    {{--inc.push(1);--}}
    {{--}else if(!ch.checked && ! items[i]['is_correct']){--}}

    {{--}--}}
    {{--}--}}
    {{--btn.setAttribute('onclick',"");--}}
    {{--var los = (100 / items.length) * inc.length;--}}
    {{--var add = (100 / cor.length) * res.length;--}}
    {{--point = point + add - los ;--}}
    {{--alert(point);--}}
    {{--}--}}
    {{--</script>--}}

    {{--@endif--}}
    {{--@endif--}}

    <script>
        var tb = document.getElementById("tb");
        typeSel(document.getElementById('sel'));

        function row() {

            var n = tb.rows.length;
            var row = tb.insertRow();
            row.id = "t" + n;
            row.innerHTML = '    <td >' + n + '</td><td><input  name="answer[is][' + n + ']" type="checkbox"></td><td><input type="text" name="answer[text][' + n + ']" placeholder="Answer" class="form-control"  ></td> <td><input type="button" class="btn btn-warning" value="X" onClick="delet(' + n + ')"/></td>';
        }

        function delet(n) {
            var row = document.getElementById("t" + n);
            tb.deleteRow(row.rowIndex);
            tableR();

        }

        function tableR() {
            for (i = 3; i < tb.rows.length; i++) {
                tb.rows[i].id = "t" + i;
                tb.rows[i].cells[0].innerHTML = i;
                tb.rows[i].cells[1].firstElementChild.setAttribute('name', 'answer[is][' + i + ']');
                tb.rows[i].cells[2].lastElementChild.setAttribute('name', 'answer[text][' + i + ']');
                tb.rows[i].cells[3].lastElementChild.outerHTML = '<input type="button" class="btn btn-warning" value="X" onclick="delet(' + i + ')">';

            }
        }

        function typeSel(select) {
            var html = document.getElementById('html');
            var quiz = document.getElementById('quiz');
            var video = document.getElementById('video');
            switch (select.selectedIndex) {
                case 0:
                    html.style.display = "block";
                    quiz.style.display = "none";
                    video.style.display = "none";
                    break;
                case 1:
                    quiz.style.display = "block";
                    video.style.display = "none";
                    html.style.display = "none";
                    break;
                case 2:
                    video.style.display = "block";
                    quiz.style.display = "none";
                    html.style.display = "none";
                    break;
            }
        }
    </script>

</div>


<div class="form-check">

    {{Form::checkbox('can_skip', Input::old('can_skip'), false,['class' => 'form-check-input'])}}
    {{ Form::label('can_skip', 'Can skip slide ?',['class','form-check-label']) }}

</div>

@if(!empty($slide))
    <div class="row">
        <div class="col">
            {{ Form::submit('Edit Slide', array('class' => 'btn btn-primary')) }}
            {{ Form::close() }}
        </div>
        <div class="col">
            {{ Form::open(array('url' => 'slide/' . $slide->id, 'class' => 'pull-right')) }}
            {{ Form::hidden('_method', 'DELETE') }}
            {{ Form::submit('Delete this Slide', array('class' => 'btn btn-warning')) }}
            {{ Form::close() }}
        </div>
    </div>

@else
    {{Form::text('topic_id',$topic->id,array('style'=>'display:none !important'))}}
    {{ Form::submit('Create Slide', array('class' => 'btn btn-primary')) }}
    {{ Form::close() }}

@endif

@endsection

@section('script')
    <script>

        var quill = new Quill('#editor-container', {
            modules: {
                toolbar: '#toolbar-container'
            },
            placeholder: 'Compose an epic...',
            theme: 'snow'
        });

        @php

        $body = trim($body);
        $body = str_replace("'","\'",$body);
         echo "   quill.root.innerHTML = ' $body ' ;";

        @endphp


        function divToForm() {
            document.getElementById('slideBody').value = quill.root.innerHTML;
        }


    </script>

@endsection


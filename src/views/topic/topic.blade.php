@extends('slide::slide-master')
@if(!empty($slide))
    @section('title', 'Topic - Edit Topic')

@else
    @section('title', 'Topic - Create Topic')

@endif

@section('top-script')
    <script src='/js/html2canvas.js'></script>
@endsection

@section('content')

    @if(!empty($topic))
        {{ Form::model($topic, array('route' => array('topic.update', $topic->id), 'method' => 'PUT')) }}


    @else
        {{ Form::open(array('url' => 'topic')) }}

    @endif


    <div class="form-group">
        {{ Form::label('title', 'Title') }}
        {{ Form::text('title', Input::old('title') , array('class' => 'form-control','placeholder' => 'Title','required'=>true)) }}
    </div>
    @if(!empty($topic))
    {{ Form::label('slides', 'Slides') }}
    <div class="form-group" id="slides">


            <a href="/slide/create/{{$topic->id}}" class="btn btn-outline-info">Create Side</a>

        @foreach ($slides as $slide)
            @if($loop->index % 2 == 0)
                <div class="row" style="margin: auto">
                    @endif
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xl-5 slide btn btn-outline-secondary">
                        @if($slide->type != "video")
                            <div id="{{$slide->id}}" class="thumb-c">

                                {!!$slide->body!!}
                            </div>
                        @else

                            <div id="{{$slide->id}}" class="thumb-c">
                                <br>
                                <br>
                                <br>
                                <h1>Video</h1>
                            </div>
                        @endif
                        <div id="s{{$slide->id}}" class="thumb btn">

                        </div>
                        <div class="buttons">
                            <span> {{$slide->title}} </span>
                            <a href="/slide/{{ $slide->id }}/edit" class="btn btn-outline-dark">Edit</a>
                            <a href="/slide/{{$slide->id}}" target="_blank" class="btn btn-primary">Show</a>
                        </div>
                        <script>
                            html2canvas(document.getElementById("{{$slide->id}}")).then(canvas => {
                                document.getElementById("{{$slide->id}}").style.display = "none";
                                document.getElementById("s{{$slide->id}}").appendChild(canvas);
                            });
                        </script>

                    </div>

                    @if($loop->index % 2 != 0 || $loop->remaining == 0)
                </div>
            @endif

        @endforeach

    </div>
    @endif


    @if(!empty($topic))
        <div class="row">
            <div class="col">
                {{ Form::submit('Edit Topic', array('class' => 'btn btn-primary')) }}
                {{ Form::close() }}
            </div>
            <div class="col">
                {{ Form::open(array('url' => 'topic/' . $topic->id, 'class' => 'pull-right')) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit('Delete this Topic', array('class' => 'btn btn-warning')) }}
                {{ Form::close() }}
            </div>
        </div>

    @else
        {{ Form::submit('Create Topic', array('class' => 'btn btn-primary')) }}
        {{ Form::close() }}

    @endif




    @endsection
@extends('slide::slide-master')
@section('title', 'Topics')


@section('content')

    @foreach ($topics as $topic)
            <div class="card">
                <div class="card-header">
                    Topic {{$topic->id}}
                </div>
                <div class="card-body">
                    <h5 class="card-title">{{$topic->title}}</h5>
                    <a href="/topic/{{$topic->id}}" class="btn btn-primary">Show</a>
                    <a href="/topic/{{$topic->id}}/edit" class="btn btn-outline-primary">Edit</a>
                </div>
            </div>

    @endforeach

@endsection

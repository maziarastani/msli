<html>
<head>
    <title>@yield('title')</title>

    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/slide.css">

</head>
<body>
@yield('top-script')

@include('slide::navbar')

<div class="container">
    @yield('content')
</div>

@yield('script')

</body>
</html>
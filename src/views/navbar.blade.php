<nav class="navbar navbar-expand-lg navbar-light bg-secondary">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="/course">Course</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/course/create">Creat course</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/chapter">Chapter</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/chapter/create">Creat Chapter</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/topic">Topic</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/topic/create">Creat Topic</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/slide">Slides</a>
            </li>
            {{--<li class="nav-item">--}}
                {{--<a class="nav-link" href="/slide/create">Creat Slide</a>--}}
            {{--</li>--}}
        </ul>
    </div>

</nav>
@extends('slide::slide-master')
@section('title', 'Chapter')


@section('content')
<h2>{{$chapter->title}}</h2>
<div class="list-group">
@foreach($topics as $topic)

        <a href="/topic/{{$topic->id}}" class="list-group-item list-group-item-action">{{$loop->index +1}}. {{$topic->title}}</a>

@endforeach
</div>

@endsection
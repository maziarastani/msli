@extends('slide::slide-master')
@if(!empty($chapter))
    @section('title', 'chapter - Edit chapter')

@else
    @section('title', 'chapter - Create chapter')

@endif

@section('content')

    @if(!empty($chapter))
        {{ Form::model($chapter, array('route' => array('chapter.update', $chapter->id), 'method' => 'PUT')) }}


    @else
        {{ Form::open(array('url' => 'chapter')) }}

    @endif


    <div class="form-group">
        {{ Form::label('title', 'Title') }}
        {{ Form::text('title', Input::old('title') , array('class' => 'form-control','placeholder' => 'Title','required'=>true)) }}
    </div>
    {{ Form::label('topics', 'topics') }}
    <div class="form-group" id="topics">
        {{--@php--}}
            {{--$to = $chapter->topic;--}}

        {{--var_dump($to);--}}

        {{--@endphp--}}
        @foreach($topics as $item)

            @php

                $checked = "";
                if(!empty($chapter)){
            if($item->chapter->contains($chapter->id)){
            $checked = "checked";
            }
            }

            @endphp
            <p><input name="topic[{{$item->id}}]" type="checkbox" {{$checked}}> -
                <span>{{$item->title}}</span></p>

        @endforeach


    </div>


    @if(!empty($chapter))
        <div class="row">
            <div class="col">
                {{ Form::submit('Edit chapter', array('class' => 'btn btn-primary')) }}
                {{ Form::close() }}
            </div>
            <div class="col">
                {{ Form::open(array('url' => 'chapter/' . $chapter->id, 'class' => 'pull-right')) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit('Delete this chapter', array('class' => 'btn btn-warning')) }}
                {{ Form::close() }}
            </div>
        </div>

    @else
        {{ Form::submit('Create chapter', array('class' => 'btn btn-primary')) }}
        {{ Form::close() }}

    @endif




@endsection
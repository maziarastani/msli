@extends('slide::slide-master')
@section('title', 'Chapters')


@section('content')

    @foreach ($chapters as $chapter)
        <div class="card">
            <div class="card-header">
                chapter {{$chapter->id}}
            </div>
            <div class="card-body">
                <h5 class="card-title">{{$chapter->title}}</h5>
                <a href="/chapter/{{$chapter->id}}" class="btn btn-primary">Show</a>
                <a href="/chapter/{{$chapter->id}}/edit" class="btn btn-outline-primary">Edit</a>
            </div>
        </div>

    @endforeach

@endsection
